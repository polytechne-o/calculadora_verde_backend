const { Router } = require("express");
const { calculoEnergia } = require("../controllers/calculo");


const router = Router();

router.post("/", calculoEnergia);


module.exports = router;
