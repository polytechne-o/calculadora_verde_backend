const express = require("express");
const cors = require("cors");

class Server {
	constructor() {
		this.app = express();
		this.port = process.env.PORT;
		this.calculoPath = "/api";

		//middlewares
		this.middleware();
		//Rutas de la aplicacion para la api
		this.routes();
	}

	routes() {
		this.app.use(this.calculoPath, require('../routes/calculo'));
		//this.app.use(this.calculoPath, require('../routes/calculo2'));
	}
	middleware() {
		// CORS
		this.app.use(cors());

		// Lectura y parseo del body
		this.app.use(express.json());

		// 
		this.app.use( express.static('public/calculadora_verde_frontend/source') );
	}

	listen() {
		this.app.listen(this.port, () => {
			console.log("Servidor corriendo en puerto", this.port);
		});
	}
}
module.exports = Server;
